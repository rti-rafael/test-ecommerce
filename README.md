# Test Ecommerce
Este projeto é destinado à plataforma de ecommerce para cadastrar e gerenciar produtos.


## Começando

Para executar o projeto, será necessário instalar os seguintes programas:

- [JDK 11: Necessário para executar o projeto Java](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
- [Maven 3++: Necessário para realizar o build do projeto Java](https://maven.apache.org/download.cgi)
- [Docker: Necessário para rodar o compose](https://www.docker.com/)
- [Git: Necessário para clonar o projeto](https://git-scm.com/)


## Desenvolvimento

Para iniciar o desenvolvimento, é necessário clonar o projeto [test-ecommerce](https://gitlab.com/rti-rafael/test-ecommerce.git) do GitLab num diretório da sua preferência:

- `test-ecommerce`: git clone https://gitlab.com/rti-rafael/test-ecommerce.git


## Construção (Build)

Para construir o projeto com o Maven, executar os comando abaixo:

- `mvn clean install` executar esse comando para baixar as dependências
- `mvn clean install -P dev` para rodar em ambiente DEV

O comando irá baixar todas as dependências do projeto e criar um diretório target com os artefatos construídos, que incluem o arquivo jar do projeto. Além disso, serão executados os testes unitários, e se algum falhar, o Maven exibirá essa informação no console.


## Tecnologias utilizadas

- [Spring-boot](https://docs.spring.io/spring-boot/docs/current/maven-plugin/usage.html)
- [Postgres](https://www.postgresql.org/)
- [Maven](https://maven.apache.org/index.html)
- [Java 11](https://www.oracle.com/java/technologies/javase-jdk8-doc-downloads.html)
- [Lombok](https://projectlombok.org/)


## Ambientes 
`http://localhost:8082/api/product` Local

`https://test-ecommerced.herokuapp.com/api/product` Produção

`https://test-ecommerced.herokuapp.com/swagger-ui/index.html` Swagger

## DB's

A aplicação necessita de bancos de dados relacionais (Postgres, ou H2 em caso de uma execução local/desenvolvimento):

- [Postgres](https://www.postgresql.org/download/)

## Boas práticas
- Sempre se certifique de não ter quebrado nenhum outro código.

- Faça build do projeto e execute-o antes de fazer commit das suas alterações.

- Utilizar o Retryable para a retentativa no código, conforme as exceções mapeadas dentro dos clients/services;

## Melhorias a serem feitas

- Quebrar a API em microserviços
- Desacoplar modulos integrados
- Convenções e padrões de projetos
- Estrutura de arquivos e pastas
- Centralizar arquivos de configurações
- Manter secrets em cofres, hoje está expostas nas properties
- Manter arquivos em Buckets na nuvem
- Criar consultas nativas para melhor desempenho da aplicação
- Detalhar mais a documentação via OpenApi



## Rodando os testes

Para rodar os testes, realize o seguinte comando

```bash
# Rodar todos os testes 
$ mvn test

# Rodar penas uma classe
$ mvn -Dtest=TestApp1 test
```


## Rodando local

Clonar o projeto no diretorio da sua preferência

```bash
  git clone https://gitlab.com/rti-rafael/test-ecommerce.git
```

Ir para o diretorio do projeto

```bash
  cd test-ecommerce
```

Instalar dependencias

```bash
  mvn clean install
```

Setar o profile conforme ambiente

```bash
  -Dspring.profiles.active=dev
```

Subir o docker compose do postgres

```bash
  docker-compose up
```

Rodar a aplicação

```bash
  mvn spring-boot:run
```


