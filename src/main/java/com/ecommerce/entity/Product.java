package com.ecommerce.entity;

import com.ecommerce.entity.Dtos.ProductRequestDto;
import com.ecommerce.entity.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id", nullable = false)
    private UUID id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToMany(mappedBy = "product")
    private List<Image> url;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

    private BigDecimal promotionalPrice;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category categories;

    @ManyToMany
    private Set<Tag> tags;

    public Product() {
    }

    public Product(ProductRequestDto dto) {
        this.title = dto.getTitle();
        this.status = dto.getStatus();
        this.description = dto.getDescription();
        this.price = dto.getPrice();
        this.promotionalPrice = dto.getPromotionalPrice();
    }
}

