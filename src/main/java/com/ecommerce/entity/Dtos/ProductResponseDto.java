package com.ecommerce.entity.Dtos;

import com.ecommerce.entity.Product;
import com.ecommerce.entity.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ProductResponseDto {

    private UUID id;

    private String title;

    private String description;

    private BigDecimal price;

    private BigDecimal promotionalPrice;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String categories;

    private Set<String> tags;


    public ProductResponseDto(Product product) {
        this.id = product.getId();
        this.title = product.getTitle();
        this.categories = product.getCategories().getName();
        this.price = product.getPrice();
        this.promotionalPrice = product.getPromotionalPrice();
        this.status = product.getStatus();
        this.description = product.getDescription();

        if (product.getTags().size() > 0) {
            this.tags = new HashSet<>();
            product.getTags().forEach(tag -> {
                this.tags.add(tag.getName());

            });
        }


    }
}
