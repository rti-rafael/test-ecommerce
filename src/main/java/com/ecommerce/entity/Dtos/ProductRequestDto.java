package com.ecommerce.entity.Dtos;

import com.ecommerce.entity.enums.Status;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDto {

    private String title;

    private String description;

    private List<String> url;

    private BigDecimal price;

    private BigDecimal promotionalPrice;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String categories;

    private List<String> tags;
}
