package com.ecommerce.entity.enums;

public enum Status {

    ATIVO, RASCUNHO;
}
