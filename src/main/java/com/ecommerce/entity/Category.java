package com.ecommerce.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "category_id", nullable = false)
    private UUID id;

    private String name;

    @OneToMany(mappedBy = "categories")
    @JsonIgnore
    private List<Product> product;

    public Category(String name, List<Product> product) {
        this.name = name;
        this.product = product;
    }

    public Category() {

    }
}
