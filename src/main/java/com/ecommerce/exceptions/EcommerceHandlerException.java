package com.ecommerce.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class EcommerceHandlerException {

    @ExceptionHandler(ProductNotNullException.class)
    public ResponseEntity<?> handleNotNullException(ProductNotNullException exc) {

        Map<String, Object> error = new LinkedHashMap<>();

        error.put("date", LocalDateTime.now());
        error.put("message", "not-null property");
        error.put("status", HttpStatus.BAD_REQUEST);
        error.put("error", exc.getMessage());
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(ObjectNotFoundException exc) {

        Map<String, Object> error = new LinkedHashMap<>();

        error.put("date", LocalDateTime.now());
        error.put("message", "Not Found Exception");
        error.put("status", HttpStatus.NOT_FOUND);
        error.put("error", exc.getMessage());
        return ResponseEntity.badRequest().body(error);
    }
}
