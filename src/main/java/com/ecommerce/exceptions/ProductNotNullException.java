package com.ecommerce.exceptions;

public class ProductNotNullException extends RuntimeException{

    public ProductNotNullException(String message, Throwable cause) {
        super(message, cause);
    }

}
