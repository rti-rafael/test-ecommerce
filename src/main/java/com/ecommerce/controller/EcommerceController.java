package com.ecommerce.controller;

import com.ecommerce.entity.Dtos.ProductRequestDto;
import com.ecommerce.service.EcommerceService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/api/product")
public class EcommerceController {

    private final EcommerceService service;

    public EcommerceController(EcommerceService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody ProductRequestDto requestDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(requestDto));
    }

    @GetMapping
    public ResponseEntity<?> findAll(Pageable page){

        return ResponseEntity.ok(this.service.findAll(page));
    }

    @GetMapping("/findByTitle")
    public ResponseEntity<?> findByTitle(@RequestParam("title") String title){

        return ResponseEntity.ok(this.service.findByTitle(title));
    }
}
