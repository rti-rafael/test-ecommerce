package com.ecommerce.service.impl;

import com.ecommerce.entity.Category;
import com.ecommerce.entity.Dtos.ProductRequestDto;
import com.ecommerce.entity.Dtos.ProductResponseDto;
import com.ecommerce.entity.Product;
import com.ecommerce.entity.Tag;
import com.ecommerce.exceptions.ObjectNotFoundException;
import com.ecommerce.exceptions.ProductNotNullException;
import com.ecommerce.repository.CategoryRepository;
import com.ecommerce.repository.ImageRepository;
import com.ecommerce.repository.ProductRepository;
import com.ecommerce.repository.TagRepository;
import com.ecommerce.service.EcommerceService;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Log4j2
public class EcommerceImpl implements EcommerceService {

    private final ProductRepository repository;

    private final CategoryRepository categoryRepository;
    private final TagRepository tagRepository;
    private final ImageRepository imageRepository;



    public EcommerceImpl(ProductRepository repository, CategoryRepository categoryRepository, ImageRepository imageRepository, TagRepository tagRepository) {
        this.repository = repository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    @Transactional
    public ProductResponseDto save(ProductRequestDto requestDto) {
        Set<Tag> tags = new HashSet<>();

        Product product = this.repository.findByTitle(requestDto.getTitle())
                .orElse(new Product(requestDto));

        Optional<Category> category = this.categoryRepository.findByName(requestDto.getCategories());

        if (category.isEmpty()) {
            Category cat = this.categoryRepository.save(new Category(requestDto.getCategories(), List.of(product)));
            product.setCategories(cat);
        } else {
            product.setCategories(category.get());
        }

        requestDto.getTags().forEach(tag -> {

            Optional<Tag> byName = this.tagRepository.findByName(tag);

            if (byName.isEmpty()) {
                Tag save = this.tagRepository.save(new Tag(tag));
                tags.add(save);
            } else {
                tags.add(byName.get());
            }
        });

        product.setTags(tags);

        try {
            return new ProductResponseDto(this.repository.save(product));
        } catch (Exception exception) {
            log.error(exception.getMessage());
            throw new ProductNotNullException(exception.getMessage(), exception);
        }
    }

    @Override
    public Page<ProductResponseDto> findAll(Pageable pageable) {

        return this.repository.findAll(pageable)
                .map(p -> new ProductResponseDto(p));
    }

    @Override
    public ProductResponseDto findByTitle(String title) {

        return this.repository.findByTitle(title)
                .map(p -> new ProductResponseDto(p))
                  .orElseThrow(() -> new ObjectNotFoundException(title + "not found"));
    }
}
