package com.ecommerce.service;

import com.ecommerce.entity.Dtos.ProductRequestDto;
import com.ecommerce.entity.Dtos.ProductResponseDto;
import com.ecommerce.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EcommerceService {

    ProductResponseDto save(ProductRequestDto product);

    Page<ProductResponseDto> findAll(Pageable pageable);

    ProductResponseDto findByTitle(String title);
}
